## Simple python speaker recognition client

This client performs a HTTP request to a Zevo speaker recognition backend.
It sends two audio files and it gets back a similarity score between [0;1]. A higher score indicate that both files may belong to the same speaker.

Running command:

    python3 speaker-recognition.py en-spkr1-file1.wav en-spkr2-file1.wav

For the example files in this folder, the similarity score must be 0.59.

Application limits:
- audio files type (wav, 1 channel, 16 bits per sample, 16 Khz);
- the API does not support simultaneous connections;
- the response time is low (23 seconds for the actual example files).
