#!/usr/bin/env python

#Usage: python file_uploader.py file1.wav file2.wav


import mimetypes
import os
import sys
from functools import partial
from uuid import uuid4

try:
    from urllib.parse import quote
except ImportError:
    # Python 2.
    from urllib import quote

from tornado import gen, httpclient, ioloop
from tornado.options import define, options


@gen.coroutine
def multipart_producer(boundary, filenames, write):
    boundary_bytes = boundary.encode()

    for filename in filenames:
        filename_bytes = filename.encode()
        mtype = mimetypes.guess_type(filename)[0] or "application/octet-stream"
        buf = (
            (b"--%s\r\n" % boundary_bytes)
            + (
                b'Content-Disposition: form-data; name="%s"; filename="%s"\r\n'
                % (filename_bytes, filename_bytes)
            )
            + (b"Content-Type: %s\r\n" % mtype.encode())
            + b"\r\n"
        )
        yield write(buf)
        with open(filename, "rb") as f:
            while True:
                # 16k at a time.
                chunk = f.read(16 * 1024)
                if not chunk:
                    break
                yield write(chunk)

        yield write(b"\r\n")

    yield write(b"--%s--\r\n" % (boundary_bytes,))


@gen.coroutine
def post(filenames):
    client = httpclient.AsyncHTTPClient(defaults=dict(request_timeout=180))
    boundary = uuid4().hex
    headers = {"Content-Type": "multipart/form-data; boundary=%s" % boundary}
    producer = partial(multipart_producer, boundary, filenames)
    response = yield client.fetch(
        "https://speaker-recognition.zevo-tech.com/post",
        method="POST",
        headers=headers,
        body_producer=producer,
    )

    print(response.body)



if __name__ == "__main__":

    filenames = options.parse_command_line()
    if not filenames:
        print("Provide 2 audio files for checking speaker identity.")
        print("python file_uploader.py file1.wav file2.wav")
        quit()

    ioloop.IOLoop.current().run_sync(lambda: post(filenames))
