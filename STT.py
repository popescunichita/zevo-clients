#!/usr/bin/env python3

import asyncio
import websockets
import sys

if len(sys.argv) != 5:
    print("Usage: STT.py <filename> <port> <api-key> <sample-rate>")
    print("<filename> path audio file")
    print("<port> 2053 for general Romanian")
    print("[optional] <sample_rate> could be 16000 or 8000; default value is 16000")
    exit(0)

filename = sys.argv[1]
port = sys.argv[2]
key = sys.argv[3]
sample_rate = sys.argv[4]

async def speechtotext(uri):
    async with websockets.connect(uri) as websocket:
        await websocket.send('{"config": {"sample_rate": "' + str(sample_rate) + '"}}')
        await websocket.send('{"config": {"key": "' + key + '"}}')
        wf = open(filename, "rb")
        while True:
            data = wf.read(16000)

            if len(data) == 0:
                break

            await websocket.send(data)
            result_text = await websocket.recv()
            print(result_text)
            if "message" in result_text:
                exit()

        await websocket.send('{"eof" : 1}')
        print (await websocket.recv())

asyncio.get_event_loop().run_until_complete(
    speechtotext('wss://live-transcriber.zevo-tech.com:' + port))
