import asyncio
import threading
import random

import asyncio
import websockets
import sys
import json
import os
from timeit import default_timer as timer

def thr(client, audioFile, output_file, port, key):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    loop.run_until_complete(transcribe(client, audioFile, output_file, port, key))
    loop.close()



async def transcribe(client, audioFile, output_file, port, key):
    full_text = ""
    async with websockets.connect('wss://live-transcriber.zevo-tech.com:' + port) as websocket:
        await websocket.send('{"config": {"sample_rate": "' + str(sample_rate) + '"}}')
        await websocket.send('{"config": {"key": "' + key + '"}}')
        wf = open(audioFile, "rb")
        while True:
            data = wf.read(16000)

            if len(data) == 0:
                break
            await websocket.send(data)
            result_text = await websocket.recv()
            if "message" in result_text:
                print(result_text)
                exit()
            result_json = json.loads(result_text)
            try:
                full_text = full_text + " " + result_json["text"]
            except:
                pass

        await websocket.send('{"eof" : 1}')
        result_text = await websocket.recv()
        result_json = json.loads(result_text)
        full_text = full_text + " " + result_json["text"]

        fileid = audioFile.rsplit('/', 1)[1].split('.')[0]
        output_file.write(fileid + " " + full_text + "\n")

        print("File " + audioFile + " successfully transcribed!")




def main():
    if len(sys.argv) != 5:
        print("Usage: STT-filelist.py <fileids_file> <port> <api-key> <sample-rate>")
        print("<fileids_file> contains full path to each audio file, one per line")
        print("<port> 2053 for general Romanian")
        print("[optional] <sample_rate> could be 16000 or 8000; default value is 16000")
        exit(0)

    fileids = sys.argv[1]
    port = sys.argv[2]
    key = sys.argv[3]
    sample_rate = sys.argv[4]


    # Read input file containing fileids
    print("Reading fileids file")
    audioFiles = []
    with open(fileids) as input_file:
        for line in input_file:
            audioFiles.append(line.splitlines())

    # Open output file
    if os.path.exists(fileids + ".transcription"):
        os.remove(fileids + ".transcription")
    output_file = open(fileids + ".transcription", "a+")

    #Starting threads
    print("Starting threads: one per each audio file")
    num_threads = len(audioFiles)
    threads = [ threading.Thread(target = thr, args=(i,audioFiles[i][0], output_file, port, key)) for i in range(num_threads) ]
    [ t.start() for t in threads ]
    [ t.join() for t in threads ]

    output_file.close()
    print("DONE!")
    print("Results in " + fileids + ".transcription")


if __name__ == "__main__":
    main()
