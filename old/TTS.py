#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests, json
import urllib3, urllib
import os, sys


if len(sys.argv) != 2:
    print("Usage: TTS.py <api-key>")
    exit(0)

key = sys.argv[1]



mydata = urllib.parse.urlencode([('voice','sam16'),('inputText',"Argentina se află în America de Sud."), ('vocoder', 'world'), ('key',key)])
#print (mydata)

tts_url = 'http://romaniantts.com/scripts/api-rotts16.php'    #the url you want to POST to
tts_response = requests.post(tts_url, data=mydata, headers={"Content-type": "application/x-www-form-urlencoded"})
#print(tts_response.text)

os.system ("wget -q " + tts_response.text)
cmd = "mplayer " + os.path.split(tts_response.text)[1]
os.system(cmd)
os.system("rm " + os.path.split(tts_response.text)[1])

