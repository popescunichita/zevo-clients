#!/usr/bin/env python3

import asyncio
import websockets
import sys

if len(sys.argv) != 5:
    print("Usage: STT.py <IP> <port> <filename> <sample rate>")
    print("IP represent the transcription server VM's IP")
    print("<port> 12320 for general Romanian general")
    print("<filename> path audio file")
    exit(0)

IP = sys.argv[1]
port = sys.argv[2]
filename = sys.argv[3]
sample_rate = sys.argv[4]

async def speechtotext(uri):
    async with websockets.connect(uri) as websocket:
        message = '{"config": {"sample_rate": "' + str(sys.argv[4]) + '"}}'
        await websocket.send(message)
        print(await websocket.recv())
        wf = open(filename, "rb")
        while True:
            data = wf.read(16000)

            if len(data) == 0:
                break

            await websocket.send(data)
            result_text = await websocket.recv()
            print(result_text)
            if "message" in result_text:
                exit()

        await websocket.send('{"eof" : 1}')
        print (await websocket.recv())

asyncio.get_event_loop().run_until_complete(
    speechtotext('ws://' + IP + ':' + port))

