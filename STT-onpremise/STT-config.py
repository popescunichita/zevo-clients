#!/usr/bin/env python3

import asyncio
import websockets
import sys


if len(sys.argv) < 4:
    print("If you want to get info from the VM:")
    print("Usage: python3 STT-config.py <IP> <port> get-info")
    print(" ")
    print("If you want to get info about the key:")
    print("Usage: python3 STT-config.py <IP> <port> get-key-info")
    print(" ")
    print("If you want to set the license key:")
    print("Usage: python3 STT-config.py <IP> <port> key <key>")
    print(" ")
    print("Please select a valid usage mode!")
    exit(0)

filename = sys.argv[0]
IP = sys.argv[1]
port = sys.argv[2]

if sys.argv[3] == "get-info":
    message = '{"config": {"get_info": " "}}'
if sys.argv[3] == "get-key-info":
    message = '{"config": {"get_key_info": " "}}'
if sys.argv[3] == "key":
    message = '{"config": {"key": "' + sys.argv[4] + '"}}'

async def speechtotext(uri):
    async with websockets.connect(uri) as websocket:
        await websocket.send(message)
        result_text = await websocket.recv()
        print(result_text)
        if "message" in result_text:
            exit()


asyncio.get_event_loop().run_until_complete(
    speechtotext('ws://' + IP +':' + port))
