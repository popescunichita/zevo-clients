#!/usr/bin/env python3
import asyncio
import websockets
import speech_recognition as sr
import requests
import warnings
import json
from urllib3.exceptions import InsecureRequestWarning
warnings.simplefilter('ignore', InsecureRequestWarning)
import urllib3
import urllib
import os
import sys
from timeit import default_timer as timer


encyclopedia = {
    "pe ce continent se află argentina": "Argentina se află în America de Sud.",
    "unde se află argentina": "Argentina se află în America de Sud.",
    "în ce an a început al doilea război mondial": "Al doilea război mondial a început în 1939.",
    "care este definiţia cuvântului onomatopee": "Cuvânt care, prin elementele lui sonore, imită sunete sau zgomote din natură.",
    "care este aria cercului": "Aria cercului este pi er pătrat.",
    "ce animal aleargă cel mai rapid": "Ghepardul este cel mai rapid animal terestru.",
    "care este teorema lui pitagora": "În orice triunghi dreptunghic, suma pătratelor catetelor este egală cu pătratul ipotenuzei."
}


# Mute ALSA output from the driver
import ctypes

ERROR_HANDLER_FUNC = ctypes.CFUNCTYPE(None, ctypes.c_char_p, ctypes.c_int,
                                      ctypes.c_char_p, ctypes.c_int,
                                      ctypes.c_char_p)


if len(sys.argv) != 3:
    print("Usage: chatbot.py <STT_api_key> <TTS_api_key>")
    exit(0)

STT_key = sys.argv[1]
TTS_key = sys.argv[2]


def py_error_handler(filename, line, function, err, fmt):
    pass


c_error_handler = ERROR_HANDLER_FUNC(py_error_handler)

try:
    asound = ctypes.cdll.LoadLibrary('libasound.so.2')
    asound.snd_lib_error_set_handler(c_error_handler)
except OSError:
    pass


async def speech_to_text_ws(audio):
    uri = 'ws://live-transcriber.zevo-tech.com:12320'
    async with websockets.connect(uri) as websocket:
        await websocket.send('{"config": {"key": "' + STT_key + '"}}')
        while (len(audio) > 0):
            data = audio[:16000]
            await websocket.send(data)
            await websocket.recv()
            audio = audio[16000:]

        await websocket.send('{"eof" : 1}')
        result_json = json.loads(await websocket.recv())
        return result_json["text"]


def text_to_speech(text):
    tts_url = 'http://romaniantts.com/scripts/api-rotts16.php'
    mydata = urllib.parse.urlencode([('voice', 'sam16'), ('inputText', text), (
        'vocoder', 'world'), ('key', TTS_key)])
    #print (mydata)

    tts_response = requests.post(tts_url, data=mydata, headers={
                                 "Content-type": "application/x-www-form-urlencoded"})
    # print(tts_response.text)

    #print("mplayer " + tts_response.text[:-1] + " > /dev/null 2>&1")
    #os.system("mplayer " + tts_response.text[:-1] + " > /dev/null 2>&1")
    return tts_response.text[:-1]


def answerQuestion():
    r = sr.Recognizer()

    with sr.Microphone(sample_rate=16000) as source:
        print('Ask a question...')
        r.pause_threshold = 1
        r.adjust_for_ambient_noise(source, duration=1)
        audio = r.listen(source)
        start_time = timer()

    try:
        # Transcribe question with Zevo speech-to-text system (LiveTranscriber)
        question = asyncio.get_event_loop().run_until_complete(speech_to_text_ws(audio.get_wav_data()))
        print('Question: ' + question)
        print("DEBUG: elapsed time " +
              "{:.2f}".format(timer() - start_time) + "s")

        # Get answer for question with dummy LUT
        try:
            answer = encyclopedia[question]
        except KeyError:
            answer = "Nu știu să răspund la această întrebare"
        print('Answer: ' + answer)

        # Get spoken answer with Zevo text-to-speech system (RomanianTTS)
        spoken_answer_url = text_to_speech(answer)
        print("DEBUG: elapsed time " +
              "{:.2f}".format(timer() - start_time) + "s")
        print("Playing audio answer...")
        os.system("mplayer " + spoken_answer_url + " > /dev/null 2>&1")
        print("DEBUG: elapsed time " +
              "{:.2f}".format(timer() - start_time) + "s")

        print("\n\n\n---------------------------")
    # loop back to continue to listen for commands if unrecognizable speech is received
    except sr.UnknownValueError:
        print('.... can`t understand')
        #command = recogniseAudioFromMic()
    return



while True:
    answerQuestion()
