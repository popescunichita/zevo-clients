### 1. Install dependencies  
`pip3 install -r requirements.txt`  

### 2. Run TTS demo  
2.0 Request a TTS_api_key from horia.cucu@zevo-tech.com  
2.1 Edit TTS.py to modify the text to be synthesized.  
2.2 Run the following command:  
`python3 TTS.py TTS_api_key`  

### 3. Run STT demo  
3.0 Request a STT_api_key from horia.cucu@zevo-tech.com  
3.1 Record a wav file to be transcribed:  
`arecord -r16000 -c1 -d5 -fS16_LE wav/telephoneNumber.wav`  
3.2 Transcribe the audio file:  
`python3 STT.py wav/telephoneNumber.wav 12320 STT_api_key`  
3.3 Transcribe the audio file if it contains formal pronunciations of numbers:  
`python3 STT.py wav/telephoneNumber.wav 12326 STT_api_key`  
3.4 Transcribe a list of audio files:  
`python3 STT-fileList.py wav/fileids 12326 STT_api_key`  

### 4. Run the chatbot demo  
4.1 View chatbot.py to see the example questions that the dummy chatbot can address.  
4.2 Run the demo asking questions in the example questions list:  
`python3 chatbot.py STT_api_key TTS_api_key`

