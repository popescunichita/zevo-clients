#!/usr/bin/env python3

import asyncio
import websockets
import time
import datetime
import sys
import numpy as np
from scipy.io.wavfile import write
from pickle import dumps, loads

if len(sys.argv) != 7:
    print("Usage: STT.py <IP> <port> <api-key> <voice> <filename>.wav <text>")
    print("Available voices: eme sam nll eigen")
    print("System no.1: IP: api-tts.zevo-tech.com port: 2083")
    print("System no.2: IP: api-tts-v02.zevo-tech.com port: 2053")
    print("python3 TTS-2021.11.py api-tts-v02.zevo-tech.com 2053 cfe79645h2ce1ce4d15jb21875078574 eme")
    exit(0)

server = sys.argv[1]
port = sys.argv[2]
key = sys.argv[3]
voice = sys.argv[4]
filename = sys.argv[5]
text = sys.argv[6]

sampling_rate = 22050

async def text2speech(uri):
    async with websockets.connect(uri) as websocket:

        message = '{"task": [{"text": "' + text + '"}, {"voice": "'+ voice + '"}, {"key": "' + key + '"}]}'
        await websocket.send(message)
        result = await websocket.recv()
        if isinstance(result, str):
            print(result)
        else:
            write(filename, sampling_rate, loads(result))
            print ("DONE!")

asyncio.get_event_loop().run_until_complete(
    text2speech('wss://' + server + ':' + port))
